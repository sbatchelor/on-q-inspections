// set namespace "cloud"
window.cloud = window.cloud || {};
console.log('loading cloud.Upload class...');

// constants
// (none)

// define class object/constructor for Upload class
cloud.Upload = function () {

    // array of file uploads
    this.uploadQueue = new Array();

    /**
    * @description tests the connection to the remote database.
    */
    this.remoteEnabled = function () {
        if (navigator.network.connection.type == Connection.NONE){
            return false;
        } else {
            return true;
        }
    }

    /**
    * @description attempts to upload picture file as attachment. on success shows a message
    * on failure the picture is then added to a queue of attachments to be uploaded when
    * connection is restored.
    */
    this.uploadToBuildingInspection = function (inspectionId, fileRef, imageData) {
        if (this.remoteEnabled()) {
            console.log('Upload.uploadToBuildingInspection -- uploading file attachment to building inspection');
            console.log('inspectionId = ' + inspectionId);
            console.log('fileRef = ' + fileRef);
            console.log('imageData = ' + imageData);
            forceClient.create(
                'Attachment',
                {
                    'ParentId': inspectionId,
                    'Name': fileRef,
                    'ContentType': 'image/jpg',
                    'Body': imageData
                },
                function (data) { // success
                    // show upload successful message?
                    console.log('Upload.uploadToBuildingInspection -- success!');
                    console.log('data: '+JSON.stringify(data));
                },
                function (error) { // error
                    // add to file upload queue? or try again? depends on error type...
                    console.log('Upload.uploadToBuildingInspection -- error');
                    console.log('error: '+JSON.stringify(error));

                    // todo: check what type of error this is and either try to re-upload or display a real error message
                    // limit re-uploads to n times
                }
            );
        } else {
            console.log('Upload.uploadToBuildingInspection -- adding file attachment to offline queue');
            this.addFileToUploadQueue(inspectionId, fileRef, imageData);
        }
    }

    /**
    * @description adds a file to the upload queue
    */
    this.addFileToUploadQueue = function (inspectionId, fileRef, imageData) {
        this.uploadQueue.push({
            'inspectionId': inspectionId,
            'fileRef': fileRef,
            'imageData': imageData
        });
    }

    /**
    * @description add listener for network reconnect event. if there are any uploads/attachments in the
    * queue then attempt to upload them
    */
    document.addEventListener(
        "online",
        function () {
            // clone upload queue into new array
            var queue = upload.uploadQueue.slice(0); // shorthand way to clone array in js
            // clear the upload queue (on failure files will be added back into the queue)
            upload.uploadQueue = new Array();
            // process each upload in the queue
            for (i in queue) {
                var item = queue[i];
                console.log('upload.uploadQueue item: '+JSON.stringify(item));
                upload.uploadToBuildingInspection(item.inspectionId, item.fileRef, item.imageData);
            }
        }
    );
};