console.log('loading app');

var forceClient; // initialized in authenticateUser()
var oauthPlugin; // salesforce oauth plugin

var db; // local and remote data transaction handler
var camera; // camera feature handle
var upload; // upload handler for attachments/images sent to salesforce
var utils; // utility functions and misc.

// warning! localStorage only stores strings!
var storage = window.localStorage;

if (storage.getItem('isAuthenticated') != 'true') {
    storage.setItem('isAuthenticated', 'false');
}

(function () {
    "use strict";

    /* Adding platform (ios/android) specific css */
    var platformStyle = document.createElement('link');
    platformStyle.setAttribute('rel', 'stylesheet');
    if (/Android/.test(navigator.userAgent)) {
        platformStyle.setAttribute('href', 'css/ratchet-theme-android.css');
    } else if (/iPhone/.test(navigator.userAgent)) {
        platformStyle.setAttribute('href', 'css/ratchet-theme-ios.css');
    }
    document.querySelector('head').appendChild(platformStyle);

    var initApp = function (afterInitCallback) {
        console.log('initializing app...');
        // init database connection interface
        db = new cloud.Database();
        camera = new cloud.Camera();
        upload = new cloud.Upload();
        utils = new cloud.Utils();
        console.log('connected to the internet: '+db.remoteEnabled());

        // get salesforce mobile sdk oauth plugin
        oauthPlugin = cordova.require("com.salesforce.plugin.oauth");

        if (storage.getItem('isAuthenticated') == 'true') {
            console.log('already authenticated!');
            // save credentials locally
            var credentials = {
                'clientId': storage.getItem('clientId'),
                'loginUrl': storage.getItem('loginUrl'),
                'accessToken': storage.getItem('accessToken'),
                'instanceUrl': storage.getItem('instanceUrl'),
                'refreshToken': storage.getItem('refreshToken')
            };
            initForceTKClient(credentials);
            afterInitCallback();
        } else {
            console.log('not authenticated! trying authentication...');
            authenticateUser(afterInitCallback, goToFailedLogin);
        }

        $('#goToInspections').on('click', function () {
            cloud.controller.InspectionList();
        });

        $('#goToLightning').on('click', function () {
            cloud.controller.Lightning();
        });
    }

    var initForceTKClient = function (credentials) {
        console.log('credentials.loginUrl: '+credentials.loginUrl);
        console.log('credentials.clientId: '+credentials.clientId);
        // Create forcetk client instance for rest API calls
        forceClient = new forcetk.Client(credentials.clientId, credentials.loginUrl);
        forceClient.setSessionToken(credentials.accessToken, "v36.0", credentials.instanceUrl);
        forceClient.setRefreshToken(credentials.refreshToken);
    }

    /**
    * @description authenticates user with Salesforce Mobile SDK's OAuth Plugin
    */
    var authenticateUser = function(successCallback, errorCallback) {
        // Call getAuthCredentials to get the initial session credentials
        oauthPlugin.getAuthCredentials(
            // Callback method when authentication succeeds.
            function (creds) {
                console.log('creds: '+JSON.stringify(creds));

                // save credentials locally
                storage.setItem('clientId', creds.clientId);
                storage.setItem('loginUrl', creds.loginUrl);
                storage.setItem('accessToken', creds.accessToken);
                storage.setItem('instanceUrl', creds.instanceUrl);
                storage.setItem('refreshToken', creds.refreshToken);

                initForceTKClient(creds);

                storage.setItem('isAuthenticated', 'true');

                // Call success handler
                successCallback();
            },
            errorCallback
        );
    }

    var goToFailedLogin = function () {
        storage.setItem('isAuthenticated', 'false');
        console.log('authentication failed!');
        // redirect to error page?
    }

    /* Wait until cordova is ready to initiate the use of cordova plugins and app launch */
    document.addEventListener("deviceready", function() {
        // init app and load building inspection list view when init completes
        initApp(cloud.controller.InspectionList);
    }, false);

})();
