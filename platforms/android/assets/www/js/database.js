// set namespace "cloud"
window.cloud = window.cloud || {};
console.log('loading cloud.Database class...');

// constants
var IS_GLOBAL_STORE = false;

// define class object/constructor for Database class
cloud.Database = function () {

    // configure the soups for local storage (in database-config.js)
    /********************** soup registrations **********************/
    // spec for building soup in local smartstore DB
    var buildingsIndexSpec = [
        {"path": "Id", "type": "string"},
        {"path": "Name", "type": "string"}
    ];

    // register the building inspection soup with smartstore
    navigator.smartstore.registerSoup(
        'Building__c',
        buildingsIndexSpec,
        function (soupName) {
            // success
            console.log('Database: registered ' + soupName + ' soup');
        },
        function () {
            // error
        }
    );

    // spec for building inspection soup in local smartstore DB
    var buildingInspectionsIndexSpec = [
        {"path": "Id", "type": "string"},
        {"path": "Name", "type": "string"},
        {"path": "Building__c", "type": "string"},
        {"path": "Inspection_Date__c", "type": "string"},
        {"path": "Summary__c", "type": "string"},
        {"path": "needsRemoteUpdate", "type": "string"}
    ];


    // register the building inspection soup with smartstore
    navigator.smartstore.registerSoup(
        'Building_Inspection__c',
        buildingInspectionsIndexSpec,
        function (soupName) {
            // success
            navigator.smartstore.clearSoup('Building_Inspection__c', function () {}, function () {});
            console.log('Database: registered ' + soupName + ' soup, cleared');
        },
        function () {
            // error
        }
    );
    /******************** end soup registrations ********************/

    // enable smartsync
    this.smartSync = cordova.require("com.salesforce.plugin.smartsync");

    /**
    * @description tests the connection to the remote database.
    */
    this.remoteEnabled = function () {
        if (navigator.network.connection.type == Connection.NONE){
            return false;
        } else {
            return true;
        }
    }

    /**
    * @description public query method. handles testing connection and returns
    * either the local cache result or the remotely queried result when
    * connection is not available.
    */
    this.query = function (queryString, successCallback) {
        if (this.remoteEnabled()) {
            this.queryRemote(queryString, successCallback);
        } else {
            this.queryLocal(queryString, successCallback);
        }
    }

    /**
    * @description private helper method for sending SOQL queries to Salesforce
    * and sends result as parameter to success callback.
    */
    this.queryRemote = function (queryString, successCallback) {
        console.log('running remote query');
        console.log('forceTK Client:');
        console.log(JSON.stringify(forceClient));
        forceClient.query(
            queryString,
            successCallback,
            function(error) {
                console.log('remote SOQL query failed:');
                console.log(queryString);
                console.log('error:');
                console.log(JSON.stringify(error));
            }
        );
    };

    /**
    * @description private helper method for querying local smartstore database
    * aconverts SOQL query to smart query and returns result set to success
    * callback function
    */
    this.queryLocal = function (queryString, successCallback) {
        console.log('running local query');
        console.log('original soql query:');
        console.log(queryString);

        // convert soql query into smart query
        // start by deconstructing SOQL query
        // get object type
        var objTypeMatch = queryString.match(/FROM ([\w]+)/i);
        var objType = objTypeMatch[1];

        // get field list
        var fieldListMatch = queryString.match(/SELECT ((([\w]+),? )+)FROM/i);
        var fieldListString = fieldListMatch[1];
        fieldListString = fieldListString.replace(/, /g, ',');
        var fields = fieldListString.split(',');

        // get where field operator value lists
        var whereClauseMatch = queryString.match(/WHERE (([\w]+) ? ([!|<|>]?=)) \'([\w]+)\'/i);
        if (whereClauseMatch) {
            var whereClauseField = whereClauseMatch[2];
            var whereClauseOperator = whereClauseMatch[3];
            var whereClauseValue = whereClauseMatch[4];
        }

        // start building smart query
        var smartQuery = 'SELECT ';
        // add fields
        for (f in fields) {
            smartQuery += '{' + objType + ':' + fields[f] + '}, ';
        }
        // remove trailing comma
        smartQuery = smartQuery.slice(0, -2) + ' ';
        smartQuery += 'FROM {' + objType + '} ';
        // add where clause
        if (whereClauseMatch) {
            smartQuery += 'WHERE {' + objType + ':' + whereClauseField + '} ';
            smartQuery += whereClauseOperator + ' ';
            smartQuery += '\'' + whereClauseValue + '\'';
        }

        console.log('smartQuery: '+smartQuery);

        var querySpec = navigator.smartstore.buildSmartQuerySpec(queryString, 100);
        // run the query
        navigator.smartstore.runSmartQuery(querySpec, successCallback);
    };

    // update function, updates locally then attempts to update remotely
    // and adds to the update queue on remote update fail
    this.update = function(objType, objId, updateFields) {
        if (this.remoteEnabled()) {
            console.log('local update to "' + objType + '" object Id=' + objId);
            this.updateLocal(objType, objId, updateFields);
            console.log('remote update to "' + objType + '" object Id=' + objId);
            this.updateRemote(objType, objId, updateFields);
        } else {
            console.log('local update to "' + objType + '" object Id=' + objId);
            updateFields['needsRemoteUpdate'] = 'true';
            this.updateLocal(objType, objId, updateFields);
            console.log('remote update delayed... waiting on connection...');
        }
    };

    this.updateRemote = function (objType, objId, updateFields) {
        console.log('update remote fields: '+JSON.stringify(updateFields));
        forceClient.update(
            objType,
            objId,
            updateFields,
            function () { // success
                console.log('remote update success');
            },
            function (error) { // error
                console.log('remote update error! error: '+JSON.stringify(error));
            }
        );
    };

    this.updateLocal = function (objType, objId, updateFields) {
        var localUpdateFields = JSON.parse(JSON.stringify(updateFields));
        localUpdateFields.Id = objId;
        console.log('updateFields:');
        console.log(JSON.stringify(localUpdateFields));
        navigator.smartstore.upsertSoupEntries(
            IS_GLOBAL_STORE,
            objType, // soup name is same as obj type eg. "Building_Inspection__c"
            [localUpdateFields],
            function () {
                console.log('local update success!');
            },
            function (error) {
                console.log('local update error! error: '+JSON.stringify(error));
            }
        );
    }

    /**
    * @description handles online state of app. attempts to push updates that have been delayed
    * todo: move the event handler function out to its own member of the database class and not an anonymous
    */
    document.addEventListener(
        "online",
        function () { // online event handler
            // query db for Building_Inspection__c objects that need remote update push
            console.log('online event fired... checking for pending remote updates...');
            //var querySpec = navigator.smartstore.buildExactQuerySpec('needsRemoteUpdate', 'true');
            //navigator.smartstore.querySoup('Building_Inspection__c', querySpec, function (result) {
            var querySpec = navigator.smartstore.buildSmartQuerySpec(
                "SELECT {Building_Inspection__c:_soup} FROM {Building_Inspection__c} WHERE {Building_Inspection__c:needsRemoteUpdate} = 'true'",
                100
            );
            navigator.smartstore.runSmartQuery(querySpec, function (result) {
                console.log('results:');
                console.log(JSON.stringify(result.currentPageOrderedEntries));
                var toUpdate = result.currentPageOrderedEntries;
                // perform the remote update
                for (i in toUpdate) {
                    console.log('update fields after reconnect: ');
                    console.log(JSON.stringify(toUpdate[i]));
                    console.log(JSON.stringify(toUpdate[i][0]));
                    db.updateRemote(
                        'Building_Inspection__c',
                        toUpdate[i][0].Id,
                        {
                            'Name': toUpdate[i][0].Name,
                            'Summary__c': toUpdate[i][0].Summary__c,
                            'Inspection_Date__c': toUpdate[i][0].Inspection_Date__c
                        }
                    );
                    // todo: unset dirty bit
                    db.updateLocal(
                        'Building_Inspection__c',
                        toUpdate[i][0].Id,
                        {
                            'Name': toUpdate[i][0].Name,
                            'Summary__c': toUpdate[i][0].Summary__c,
                            'Inspection_Date__c': toUpdate[i][0].Inspection_Date__c
                        }
                    );
                }
            });
        },
        false
    );
}