// set namespace "cloud.controller"
window.cloud = window.cloud || {};
window.cloud.controller = window.cloud.controller || {};
console.log('loading cloud.controller.Inspection class...');

// constants
// (none)

/**
* @description building inspection detail view controller
*/
cloud.controller.Inspection = function (inspectionId) {
    /**
    * @description constructor for the building inspection list view
    */
    this.init = function() {
        this.inspectionId = inspectionId;
        this.viewSrc = '/views/inspection.html';
        this.buildingInspectionDetails = new Array();
        this.loadView();
        window.controller = this;
    }

    /**
    * @description loads the view html into the container
    */
    this.loadView = function () {
        console.log('loading view from: '+cordova.file.applicationDirectory + "www" + this.viewSrc);
        window.resolveLocalFileSystemURL(
            cordova.file.applicationDirectory + "www" + this.viewSrc,
            function (viewFile) { // file found
                // read the view file
                viewFile.file(function(file) {
                    var reader = new FileReader();
                    reader.onload = function() {
                        // load in and render view source
                        console.log("view source: ");
                        console.log(reader.result);
                        console.log('source length' + reader.result.length);
                        $('#content').html(reader.result);
                        window.controller.initView();
                    }
                    reader.readAsText(file);
                });
            },
            function (error) {
                console.log('error: failed to load building inspection view file! '+JSON.stringify(error)+'');
            }
        );
    }

    /**
    * @description initializes building inspection list view controller
    */
    this.initView = function () {
        console.log('cloud.controller.inspection.initView start');
        /*************************** set title bar ******************************/
        $('#title').text('Building Inspection Details');
        $('#titleBar').prepend('<button id="backBtn" class="btn btn-link btn-nav pull-left">'+
            '<span class="icon icon-left-nav"></span>'+
            'Back'+
            '</button>'
        );

        $('#backBtn').on('click', function () {
            // switch back to the inspection list view
            cloud.controller.InspectionList();
        });
        /************************* end set title bar ****************************/

        /****************** register interface event handlers *******************/
        $("#save").on('click', function () {
            db.update(
                'Building_Inspection__c',
                window.controller.inspectionId,
                {
                    'Name': $('#Name').val(),
                    //'Inspection_Date__c': $('#Inspection_Date__c').val(), // todo: figure out how to serialize dates to salesforces standards
                    'Summary__c': $('#Summary__c').val()
                }
            );
        });

        $('#showPic').on('click', function () {

        });

        $('#takePic').on('click', function () {
            window.camera.takePicture(function (imageUri) {
                // add the picture to the beginning picture collection
                var imageContainer = '<li class="table-view-cell media">';
                imageContainer += '<img class="media-object pull-left inspection-image" src="'+imageUri+'" />';
                imageContainer += '</li>';
                $('#pics').prepend(imageContainer);

                // get raw image data from file so we can stream it to salesforce
                window.camera.getRawData(imageUri, function (imageData) {
                   // attach picture to inspection object in salesforce
                   window.upload.uploadToBuildingInspection(
                       window.controller.inspectionId,
                       imageUri,
                       imageData
                   );
                });
            });
        });
        /**************** end register interface event handlers *****************/

        // populate list view
        this.showDetails();
        this.showImages();
    }

    /**
    * @description queries for details of building inspection and displays results in a single column
    * list view
    */
    this.showDetails = function() {
        console.log('showDetails:');

        var soql = 'SELECT ' +
            'Id, ' +
            'Name, ' +
            'Building__c, ' +
            'Inspection_Date__c, ' +
            'Summary__c ' +
            'FROM Building_Inspection__c ' +
            'WHERE Id = \''+this.inspectionId+'\' '+
            'LIMIT 1';

        db.query(soql, function(result) {
            console.log('showDetails: soql query success callback');
            var details = result.records[0];

            if (details.Inspection_Date__c == null) {
                details.Inspection_Date__c = '';
            }
            if (details.Summary__c == null) {
                details.Summary__c = '';
            }

            var detailsHtml = '';
            detailsHtml += '<li class="table-view-cell">' +
                '<div class="media-body">Name:' +
                '<input id="Name" type="text"' +
                ' value="' + details.Name + '"/>' +
                '</div>'+
                '</li>';
            detailsHtml += '<li class="table-view-cell">' +
                '<div class="media-body">Date:' +
                '<input id="Inspection_Date__c" type="text"' +
                ' value="' + details.Inspection_Date__c + '"/>' +
                '</div>'+
                '</li>';
            detailsHtml += '<li class="table-view-cell">' +
                '<div class="media-body">Summary:' +
                '<input id="Summary__c" type="text"' +
                ' value="' + details.Summary__c + '"/>' +
                '</div>'+
                '</li>';
            $('#buildingInspectionDetails').html(detailsHtml);
        });
    }

    /**
    * @description queries for attached images and displays them on the page
    */
    this.showImages = function () {

        console.log('showImages:');

        var soql = 'SELECT ' +
            'Id, ' +
            'Body, ' +
            'BodyLength, ' +
            'Description, ' +
            'ContentType ' +
            'FROM Attachment ' +
            'WHERE ParentId = \'' + this.inspectionId + '\' '+
            'AND ContentType LIKE \'image%\'';


        db.query(soql, function(result) {
            console.log('showImages: soql query success callback');
            console.log(JSON.stringify(result));
            var images = result.records;

            for (i in images) {
                console.log('image: ');
                console.log(JSON.stringify(images[i]));

                var contentType = images[i].ContentType;
                var contentServerUrl = forceClient.instanceUrl;
                contentServerUrl = contentServerUrl.replace('https://', 'https://c.');
                contentServerUrl = contentServerUrl.replace('salesforce.com', 'content.force.com')

                var imageContainer = '<li class="table-view-cell media">';
                imageContainer += '<img class="media-object pull-left inspection-image" src="' + contentServerUrl + '/servlet/servlet.FileDownload?file=' + images[i].Id + '" />';
                imageContainer += '</li>';

                $('#pics').append(imageContainer);

            }

        });
    }

    this.init();
}