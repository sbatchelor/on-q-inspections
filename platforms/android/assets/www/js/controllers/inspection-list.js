// set namespace "cloud.controller"
window.cloud = window.cloud || {};
window.cloud.controller = window.cloud.controller || {};
console.log('loading cloud.controller.InspectionList class...');

// constants
// (none)

/**
* @description building inspection list view controller
*/
cloud.controller.InspectionList = function () {

    /**
    * @description constructor for the building inspection list view
    */
    this.init = function() {
        this.viewSrc = '/views/inspection-list.html';
        this.buildingInspectionList = new Array();
        this.loadView();
        window.controller = this;
    }

    /**
    * @description loads the view html into the container
    */
    this.loadView = function () {
        console.log('loading view from: '+cordova.file.applicationDirectory + "www" + this.viewSrc);
        window.resolveLocalFileSystemURL(
            cordova.file.applicationDirectory + "www" + this.viewSrc,
            function (viewFile) { // file found
                // read the view file
                viewFile.file(function(file) {
                    var reader = new FileReader();
                    reader.onload = function() {
                        // load in and render view source
                        console.log("view source: ");
                        console.log(reader.result);
                        console.log('source length' + reader.result.length);
                        $('#content').html(reader.result);
                        window.controller.initView();
                    }
                    reader.readAsText(file);
                });
            },
            function (error) {
                console.log('error: failed to load building inspection list view file! '+JSON.stringify(error)+'');
            }
        );
        this.viewSource
    }

    /**
    * @description initializes building inspection list view controller
    */
    this.initView = function () {
        /*************************** set title bar ******************************/
        $('#titleBar').children('button').remove(); // clear the buttons from the title bar
        $('#title').text('Building Inspection List');
        /************************* end set title bar ****************************/

        /****************** register interface event handlers *******************/

        /**************** end register interface event handlers *****************/

        // populate list view
        this.showBuildingInspectionsList();
    }

    /**
    * @description queries for list of building inspections and displays results in a single column
    * list view
    */
    this.showBuildingInspectionsList = function() {
        console.log('showBuildingInspectionsList:');
        var soql = 'SELECT Id, Name, Building__c, Inspection_Date__c, Summary__c FROM Building_Inspection__c LIMIT 10';

        db.query(soql, function(result) {
            console.log('showBuildingInspectionsList: soql query success callback');
            buildingInspectionList = result.records;
            var listItemsHtml = '';
            for (var i=0; i < buildingInspectionList.length; i++) {
                listItemsHtml += '<li class="table-view-cell">' +
                    '<div class="media-body inspection-link" id="' + buildingInspectionList[i].Id + '">' +
                    buildingInspectionList[i].Name +
                    '</div>' +
                    '</li>';
            }

            $('#buildingInspections').html(listItemsHtml);

            $('.inspection-link').on('click', function (ev) {
                // load the inspection detail view pass in the inspection id
                cloud.controller.Inspection($(this).attr('id'));
            });
        });
    }

    this.init();
}