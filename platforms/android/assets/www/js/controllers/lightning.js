// set namespace "cloud.controller"
window.cloud = window.cloud || {};
window.cloud.controller = window.cloud.controller || {};
console.log('loading cloud.controller.Lightning class...');

// constants
// (none)

/**
* @description lightning interface
*/
cloud.controller.Lightning = function () {
    /**
    * @description constructor for the building inspection list view
    */
    this.init = function() {
        this.viewSrc = 'https://onqcloud--clouddev.lightning.force.com/one/one.app';

        /*
        $('#content').load(this.viewSrc, '', function () {
            $.ajaxPrefilter(function( options ) {
              if ( options.crossDomain ) {
                options.url = "https://onqcloud--clouddev.lightning.force.com/" + encodeURIComponent( options.url );
                options.crossDomain = true;
              }
            });
            $('#content').html($('#content').html().replace(/href=\"/g, 'href="https://onqcloud--clouddev.lightning.force.com'));
            $('#content').html($('#content').html().replace(/src=\"/g, 'href="https://onqcloud--clouddev.lightning.force.com'));
        });
        */

        window.location = this.viewSrc;
    }

    this.init();
}